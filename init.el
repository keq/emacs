;; -*- lexical-binding: t; -*-

;;
;; Enforce XDG Base Dir Spec
;;
(require 'xdg)
(defun my/xdg (base-type &optional name)
  "Constructs a path based on a given XDG base dir (`base-type`) and name for the final part of the path (`name`).
Currently `base-type` may be one of: ('config', 'cache', 'data')."
  (unless name (setq name ""))
  (let ((base-type
         (cond
          ((eq base-type :config) (xdg-config-home))
          ((eq base-type :cache) (xdg-cache-home))
          ((eq base-type :data) (xdg-data-home))
          (t (error "%s is not a supported base-type" base-type)))))
    (expand-file-name name (expand-file-name "emacs" base-type))))

;; built-in
(setq auto-save-file-name-transforms `((".*" ,(my/xdg :data) t)))
(setq custom-file (my/xdg :config "custom.el"))
(setq package-user-dir (my/xdg :data "elpa"))
(when (boundp 'native-comp-eln-load-path)
  (setcar native-comp-eln-load-path (my/xdg :data "eln-cache")))
;; packages
(setq straight-base-dir (my/xdg :data))
(setq lsp-session-file (my/xdg :data "lsp/session.el"))
(setq svg-lib-icons-dir (my/xdg :cache "svg-lib"))
(setq no-littering-etc-directory (my/xdg :config))
(setq no-littering-var-directory (my/xdg :data))


;;
;; Bootstrap straight.el
;;

;; https://github.com/radian-software/straight.el#summary-of-options-for-package-modification-detection
(setq straight-check-for-modifications '(watch-files find-when-checking))
(defvar bootstrap-version)
(let ((bootstrap-file
       (my/xdg :data "straight/repos/straight.el/bootstrap.el"))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; install `use-package`
(straight-use-package 'use-package)
;; install packages by their name in `use-package` declarations
;; unless otherwise specified by `:straight` keyword
(setq straight-use-package-by-default t)

;; defer package loading by default
(setq use-package-always-defer t)

;; more comprehensive XDG base dir spec respec
(use-package no-littering :demand t
  :config (no-littering-theme-backups))

;; general emacs stuff
(use-package emacs
  :config
  (defalias 'yes-or-no-p 'y-or-n-p)

  (setq backup-by-copying t)
  (setq delete-old-versions t
        kept-new-versions 6
        kept-old-versions 2
        version-control t)

  ;; Save cursor place
  (save-place-mode 1)

  (setq read-process-output-max (* 1 1024 1024))

  (global-visual-line-mode 1)
  (setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))
  (setq display-line-numbers-type 'relative)
  (add-hook 'prog-mode-hook 'display-line-numbers-mode)

  (setq-default indent-tabs-mode nil)
  (setq-default tab-width 4)
  ;; single space between sentences when filling
  (setq-default sentence-end-double-space nil)

  (prefer-coding-system 'utf-8)
  (set-default-coding-systems 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)

  (setq confirm-kill-processes nil)

  ;; don't ask, just go
  (setq vc-follow-symlinks t)

  (setq inhibit-eol-conversion t)

  (column-number-mode 1)

  (setq shell-file-name "/bin/bash")

  (setq clean-buffer-list-delay-general 1)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  (setq read-extended-command-predicate
        #'command-completion-default-include-p)

  (setq enable-recursive-minibuffers t)

  ;; prevent comp warnings from constantly popping up
  (setq native-comp-async-report-warnings-errors nil)

  ;;
  ;; SOMEWHAT GENERAL FUNCTIONS ;;
  ;;
  (defun my/append-to-list (list-var elements)
    "Append ELEMENTS to the end of LIST-VAR.
The return value is the new value of LIST-VAR."
    (unless (consp elements)
      (error "ELEMENTS must be a list"))
    (let ((list (symbol-value list-var)))
      (if list
          (setcdr (last list) elements)
        (set list-var elements)))
    (symbol-value list-var))

  (defun my/async-shell-command-no-window
      (command)
    (interactive)
    (let
        ((display-buffer-alist
          (list
           (cons
            "\\*Async Shell Command\\*.*"
            (cons #'display-buffer-no-window nil)))))
      (async-shell-command
       command)))

  ;;
  ;; AESTHETICS
  ;;
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  ;; to avoid errors in termux
  (when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

  (cond
   ((string= (system-name) "deep") (setq font-height 140))
   ((string= (system-name) "bill") (setq font-height 160))
   (t (setq font-height 140)))

  (set-face-attribute 'default nil :family "Iosevka" :height font-height)

  (defun my/transparent-in-terminal ()
    (unless (display-graphic-p (selected-frame))
      (set-face-background 'default "unspecified-bg" (selected-frame))))
  (add-hook 'window-setup-hook 'my/transparent-in-terminal)

  (setq inhibit-startup-screen t)

  (add-to-list 'display-buffer-alist
               '("\\*.*\\*"
                 display-buffer-at-bottom nil
                 (window-height . 0.3)))
  (push '("\\.tsx\\'" . tsx-ts-mode) auto-mode-alist)
  (setq treesit-font-lock-level 4))

(use-package gcmh
  :demand t
  :config
  (setq gcmh-idle-delay 'auto
        gcmh-auto-idle-delay-factor 10
        gcmh-high-cons-threshold (* 512 1024 1024))
  (gcmh-mode 1))

(use-package explain-pause-mode
  :straight (explain-pause-mode :type git :host github :repo "lastquestion/explain-pause-mode"))

(use-package esup
  :commands esup
  :config
  (setq esup-depth 0))

;; correct indentation for wrapped lines
(use-package adaptive-wrap
  :hook (visual-line-mode . adaptive-wrap-prefix-mode))

(use-package project
  :straight (:type built-in)
  :config
  (defcustom my/project-markers '(".project")
    "Filenames that mark a directory as a project."
    :type '(repeat (string :tag "Filename")))

  (my/append-to-list 'my/project-markers '("package.json" "Cargo.toml" ".venv" "pyproject.toml" "setup.py"))

  (defun my/project-find (dir)
    "Find the project root directory based on project marker files (e.g. package.json, .venv)."
    (if-let ((root
              (seq-some (lambda (n)
                          (locate-dominating-file dir n))
                        my/project-markers))
             (pr
              (list 'vc 'Git root)))
        (progn
          (project-remember-project pr)
          pr)))
  (advice-add 'project-try-vc :before-until #'my/project-find)

  (setq project-vc-merge-submodules nil))

(use-package dashboard
  :demand t
  :config
  (add-to-list 'recentf-exclude (expand-file-name "~/.local/share/emacs/bookmark-default.el"))
  (setq dashboard-startup-banner 3)
  (setq dashboard-center-content t)
  (setq dashboard-projects-backend 'project-el)
  (setq dashboard-projects-switch-function 'dired)
  (setq dashboard-items
        '(
          (recents . 10)
          (projects . 10)
          (bookmarks . 10)))
  (setq dashboard-set-footer nil)
  (dashboard-setup-startup-hook))

;;
;; GENERAL/UNIVERSAL KEYBINDINGS
;;

;; https://github.com/hlissner/doom-emacs/blob/develop/modules/config/default/+evil-bindings.el
(use-package general
  :demand t
  :config
  (general-create-definer override-def
    :states '(normal visual motion)
    :keymaps 'override)
  (general-create-definer spc-leader-def
    :prefix "SPC"
    :states '(normal visual motion))
  (general-create-definer spc-leader-override-def
    :prefix "SPC"
    :states '(normal visual motion)
    :keymaps 'override)

  (override-def
    ";"  'execute-extended-command)
  (spc-leader-override-def
    "R"  'restart-emacs
    "Q"  'save-buffers-kill-emacs
    "q"  'save-buffers-kill-terminal
    "mm"  'bookmark-set
    "mb"  'bookmark-jump
    "md"  'bookmark-delete
    "h"  'help-command
    "bb" 'switch-to-buffer
    "br" 'revert-buffer
    "bd" 'kill-this-buffer
    "bs" 'basic-save-buffer
    "bc" 'clean-buffer-list
    "ff" 'find-file
    "fs" 'save-buffer
    "fS" 'write-file
    "d" 'dired-jump
    "p" '(:keymap project-prefix-map :wk "project"))

  ;; esc always quits
  (keymap-set key-translation-map "ESC" "C-g")

  )

(use-package orderless
  :ensure t
  :init
  (setq completion-styles '(orderless basic)
        completion-category-overrides '((file (styles basic partial-completion)))
        orderless-matching-styles '(orderless-flex))
  )

(use-package vertico
  :init
  (vertico-mode))

(use-package counsel
  :general
  (spc-leader-override-def
    "ff" 'counsel-find-file))

(use-package consult
  :general
  (spc-leader-override-def
    "/" 'consult-line
    "bb" 'consult-buffer))

(use-package vertico-grid
  :straight nil
  :after vertico)

(use-package company
  :init
  (add-hook 'prog-mode-hook 'global-company-mode)
  :config
  (setq company-minimum-prefix-length 1)
  (setq company-idle-delay 0.1)
  (setq company-backends '((company-capf company-yasnippet))))

(use-package marginalia
  :demand t
  :config
  (marginalia-mode))

(use-package undo-fu
  :demand t)
(use-package undo-fu-session
  :demand t
  :config
  (global-undo-fu-session-mode))

(use-package evil
  :hook (after-init . evil-mode)
  :requires undo-fu
  :general
  ( :states '(normal visual motion)
    "j" 'evil-next-visual-line
    "k" 'evil-previous-visual-line
    "0" 'evil-beginning-of-visual-line
    "$" 'evil-end-of-visual-line)
  (spc-leader-override-def
    "w" 'evil-window-map
    "wd" 'kill-buffer-and-window)
  :init
  ;; all must be set before loading
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-integration t)
  :config
  (evil-set-undo-system 'undo-fu))

(use-package god-mode :demand t)
(use-package evil-god-state
  :demand t
  :general
  (spc-leader-override-def
    "SPC" 'evil-execute-in-god-state))

(use-package evil-collection
  :custom
  (evil-collection-key-blacklist '("gu" "gl"))
  :hook
  (evil-mode . evil-collection-init)
  (evil-collection-setup
   . (lambda (&rest _) (general-define-key
                        :keymaps 'magit-mode-map
                        :states 'normal
                        "gu" 'magit-smerge-keep-upper
                        "gl" 'magit-smerge-keep-lower))))

(use-package evil-commentary
  :demand t
  :after evil
  :config
  (evil-commentary-mode))

(use-package evil-surround
  :demand t
  :after evil
  :config
  (global-evil-surround-mode 1))

(use-package evil-mc
  :demand t
  :general
  ( :states 'visual
    "A" #'evil-mc-make-cursor-in-visual-selection-end
    "I" #'evil-mc-make-cursor-in-visual-selection-beg)
  ;; for some reason I can't figure out how to make escape always
  ;; output keyboard-quit so this will have to do:
  ( :states 'normal
    :keymap 'evil-mc-map
    "<escape>" #'evil-mc-undo-all-cursors)
  :config
  (global-evil-mc-mode 1))

;; recreate nnn
(use-package dired
  :straight (:type built-in)
  :general
  ( :states 'normal
    :keymaps 'dired-mode-map
    "h" 'my/dired-up-directory
    "j" 'dired-next-line
    "k" 'dired-previous-line
    "l" 'my/dired-open
    "e" 'dired-find-file
    "r" 'dired-do-redisplay
    "/" 'dired-narrow
    "nf" 'dired-create-empty-file
    "nd" 'dired-create-directory
    "." 'dired-hide-dotfiles-mode)
  :config
  (defun my/xdg-open (file)
    "Open file with xdg-open asynchronously in a detached process."
    (interactive)
    (let
        ((command (format "xdg-open %s" (file-truename file))))
      (message command)
      (start-process "my/xdg-open" nil "/bin/bash" "-c" (format "nohup 1>/dev/null 2>/dev/null %s" command))))
  (defun my/dired-open ()
    "Basically `dired-find-file' but replaces the buffer when navigating to a directory, and uses `my/xdg-open' for files.

This is so that file system navigation does not create a bunch of buffers.

It uses `find-alternate-file' to open directories in the current buffer.
"
    (interactive)
    (let ((find-file-run-dired t)
          (file (dired-get-file-for-visit)))
      (if (file-directory-p file)
          (find-alternate-file file)
        (my/xdg-open file))))
  (defun my/dired-up-directory (&optional other-window)
    "Run Dired on parent directory of current directory."
    (interactive "P")
    (let* ((dir (dired-current-directory))
           (orig (current-buffer))
           (up (file-name-directory (directory-file-name dir))))
      (or (dired-goto-file (directory-file-name dir))
          ;; Only try dired-goto-subdir if buffer has more than one dir.
          (and (cdr dired-subdir-alist)
               (dired-goto-subdir up))
          (progn
            (kill-buffer orig)
            (dired up)
            (dired-goto-file dir)))))
  (use-package dired-narrow)
  (use-package dired-hide-dotfiles)

  (setq dired-listing-switches "-lahG1v --time-style long-iso --group-directories-first")
  (if (eq system-type 'darwin)
      (setq insert-directory-program "gls" dired-use-ls-dired t))

  (add-hook 'dired-mode-hook #'hl-line-mode)
  (add-hook 'dired-mode-hook #'dired-hide-dotfiles-mode))

(use-package magit
  :demand t
  :general
  (spc-leader-override-def
    "g" 'magit)
  ( :keymaps 'transient-base-map
    "<escape>" 'transient-quit-one)
  :config
  (setq transient-default-level 7)
  (custom-set-faces
   '(magit-diff-added-highlight ((((type tty)) (:foreground "LimeGreen"))))
   '(magit-diff-context-highlight ((((type tty)) (:foreground "default"))))
   '(magit-diff-file-heading ((((type tty)) nil)))
   '(magit-diff-removed ((((type tty)) (:foreground "red" :background nil))))
   '(magit-diff-removed-highlight ((((type tty)) (:foreground "red" :background nil))))
   '(magit-section-highlight ((((type tty)) nil)))))

(use-package diff-hl
  :demand t
  :config
  ;; prevent bookmarks' fringe marks conflicts
  (setq bookmark-set-fringe-mark nil)

  (setq diff-hl-flydiff-delay 0)
  (setq diff-hl-show-staged-changes nil)
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)

  (global-diff-hl-mode)
  (diff-hl-flydiff-mode))

(use-package which-key
  :demand t
  :config
  (which-key-enable-god-mode-support)
  (which-key-mode))

(use-package vterm
  :general
  (spc-leader-override-def
    "t" 'my/vterm-toggle
    "T" 'my/vterm-toggle-cd)
  ( :keymaps 'vterm-mode-map
    :states 'insert
    "<return>" 'vterm-send-return)
  :init
  (defun my/vterm-hide ()
    (if (one-window-p)
        (mode-line-other-buffer)
      (quit-window)))
  (defun my/vterm-toggle (arg)
    (interactive "P")
    (cond ((and (derived-mode-p 'vterm-mode)
                (or
                 (not arg)
                 (string= (buffer-name) (format "*vterm*<%s>" arg))))
           (my/vterm-hide))
          ((derived-mode-p 'vterm-mode)
           (my/vterm-hide)
           (vterm arg))
          (t (vterm arg))))

  (defun my/vterm-toggle-cd (arg)
    (interactive "P")
    (let ((cd-str default-directory))
      (my/vterm-toggle arg)
      (if (and
           (eq major-mode 'vterm-mode)
           (not (string= cd-str default-directory)))
          (progn
            (vterm-send-C-a)
            (vterm-send-C-k)
            (vterm-send-string cd-str t)
            (vterm-send-return)))))

  :config
  (setq vterm-module-cmake-args "-DUSE_SYSTEM_LIBVTERM=no")
  (setq vterm-always-compile-module t)
  (setq vterm-max-scrollback 10000)
  (setq vterm-shell "fish"))

(use-package apheleia
  :init
  ;; > The autoloading has been configured so that this will not cause
  ;; > Apheleia to be loaded until you save a file.
  (apheleia-global-mode +1))

(use-package aggressive-indent
  :hook
  (emacs-lisp-mode . aggressive-indent-mode))

(use-package yasnippet
  :hook
  ((rustic-mode python-mode c-mode tex-mode js-mode web-mode mhtml-mode org-mode typescript-mode) . yas-minor-mode)
  :config
  (yas-reload-all))

(use-package rainbow-delimiters :hook (prog-mode . rainbow-delimiters-mode))

(use-package doom-themes
  :demand t
  :config
  (setq evil-normal-state-cursor `(,(doom-color 'green) box))
  (setq evil-insert-state-cursor `(,(doom-color 'blue) bar))
  (setq evil-visual-state-cursor `(,(doom-color 'yellow) box))
  (setq evil-replace-state-cursor `(,(doom-color 'red) hbar))
  (setq evil-operator-state-cursor `(,(doom-color 'fg) evil-half-cursor))
  (setq evil-emacs-state-cursor   `(,(doom-color 'magenta) bar))
  (doom-themes-visual-bell-config)
  (doom-themes-org-config)
  (load-theme 'doom-one t)

  (custom-set-faces
   `(dired-directory ((t (:foreground ,(doom-color 'blue)))))
   `(completions-common-part
     ((t ( :foreground ,(doom-color 'magenta)
           :background ,(doom-color 'base1)
           :weight semi-bold))))))

(use-package diredfl
  :demand t
  :config
  (set-face-foreground 'diredfl-date-time (doom-color 'dark-cyan))
  (set-face-foreground 'diredfl-number (doom-color 'green))
  (diredfl-global-mode))

(use-package all-the-icons
  :config
  (defun my/font-installed-p (font-name)
    "Check if font with FONT-NAME is available."
    (if (find-font (font-spec :name font-name))
        t
      nil))

  (when (and (not (my/font-installed-p "all-the-icons"))
             (window-system))
    (all-the-icons-install-fonts t)))

(use-package minions
  :demand t
  :config
  (minions-mode 1))

(use-package doom-modeline
  :demand t
  :init
  (setq doom-modeline-project-detection 'project)
  (setq doom-modeline-minor-modes t)
  :config
  (doom-modeline-mode 1))

(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package dired-sidebar
  :commands dired-sidebar-toggle-sidebar
  :hook
  (dired-sidebar-mode . (lambda ()
                          (unless (file-remote-p default-directory)
                            (auto-revert-mode))))
  :general
  (spc-leader-override-def
    "s" 'dired-sidebar-toggle-sidebar)
  ( :states 'normal
    :keymaps 'dired-sidebar-mode-map
    "h" 'dired-sidebar-up-directory
    "l" 'my/dired-sidebar-open)
  :init
  (setq dired-sidebar-theme 'icons)
  (setq dired-sidebar-one-instance-p 1)

  (defun my/dired-sidebar-open ()
    "Basically `dired-sidebar-find-file' but uses `my/xdg-open' for files."
    (interactive)
    (let ((find-file-run-dired t)
          (file (dired-get-file-for-visit)))
      (if (file-directory-p file)
          (dired-sidebar-find-file file)
        (my/xdg-open file)))))

;; this is for GTK css where css-ls breaks
(use-package rainbow-mode
  :hook css-mode)

;;
;; LANGS
;;
(use-package org
  :straight (org :type built-in)
  :general
  (spc-leader-def
    :keymaps 'org-mode-map
    "oc" 'org-capture
    "or" 'org-refile
    "oa" 'org-archive-subtree
    "a" 'org-agenda-list
    "os" 'my/org-reveal-insert-slide)
  :config
  (setq org-startup-indented t)
  (setq org-hide-emphasis-markers t)

  (defun my/org-reveal-insert-slide ()
    (interactive)
    (end-of-line)
    (insert "\n* \n#+BEGIN_NOTES\n#+END_NOTES\n * ")
    (previous-line 3))

  (setq org-agenda-span 31)

  (setq org-format-latex-options
        '(:foreground default :background default :scale 2.0 :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers
                      ("begin" "$1" "$" "$$" "\\(" "\\[")))

  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (latex . t)))

  (use-package ox-reveal
    :config
    (setq org-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js"))

  (use-package evil-org
    :after org
    :config
    (require 'evil-org-agenda)
    (evil-org-agenda-set-keys)
    (setq org-M-RET-may-split-line nil)
    :hook
    (
     (org-mode . evil-org-mode)
     (evil-org-mode . evil-org-set-key-theme))))

;; LSP
(use-package lsp-mode
  :general
  (spc-leader-override-def
    "l" '(:keymap lsp-command-map :wk "lsp"))
  :init
  (defun my/install-lsp-format-hook ()
    (add-hook 'before-save-hook #'lsp-format-buffer nil 'local))
  (add-hook 'latex-mode-hook #'my/install-lsp-format-hook)
  (add-hook 'prisma-mode-hook #'my/install-lsp-format-hook)

  (add-hook 'typescript-ts-mode-hook (lambda () (lsp-ensure-server 'eslint)))
  (add-hook 'typescript-ts-mode-hook (lambda () (lsp-ensure-server 'ts-ls)))
  (add-hook 'prisma-mode-hook (lambda () (lsp-ensure-server 'prismals)))

  (setq lsp-warn-no-matched-clients nil)

  :hook
  ((prog-mode . lsp-deferred)
   (lsp-mode . lsp-enable-which-key-integration))
  :commands
  lsp
  :config
  (setq lsp-use-plists t)
  (setq lsp-disabled-clients '(pyls rls deno-ls))

  (add-to-list 'lsp-language-id-configuration '(".*\\.ejs$" . "html"))

  (setq lsp-eslint-server-command `("vscode-eslint-language-server" "--stdio"))

  (setq lsp-enable-file-watchers nil)
  ;; (setq lsp-log-io t)
  (setq lsp-rust-server 'rust-analyzer)
  (setq lsp-rust-analyzer-server-command '("rust-analyzer"))
  (setq lsp-rust-clippy-preference 'on))

(use-package lsp-ui :commands lsp-ui-mode
  :config (setq evil-lookup-func 'lsp-ui-doc-show))

(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "pandoc")
  :config
  (setq markdown-fontify-code-blocks-natively t))

;; LaTeX
(use-package tex-mode
  :straight (:type built-in)
  :general
  (spc-leader-def
    :keymaps 'latex-mode-map
    "cc"
    (lambda ()
      (interactive)
      (my/async-shell-command-no-window
       (format "lualatex -interaction nonstopmode %s"
               (shell-quote-argument (buffer-file-name))))))
  :config
  (set-face-attribute 'tex-verbatim
                      nil
                      :family "monospace"))

;; Rust
(use-package toml-mode
  :mode ("\\.toml\\'"))

;; Python
(use-package python
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python" . python-mode)
  :config
  (setq python-shell-interpreter "python3"))

(use-package pyvenv
  ;; my/pyvenv-autoload must be run before anything else that may be
  ;; affected by venv. Seemingly hook functions are run in reverse
  ;; order of insertion so this is hooked after pyright.
  :hook (python-mode . my/pyvenv-autoload)
  :init
  (defun my/pyvenv-autoload ()
    "Automatically find and activate nearest .venv"
    (f-traverse-upwards
     (lambda (path)
       (let ((venv-path (f-expand ".venv" path)))
         (if (f-exists? venv-path)
             (progn
               (pyvenv-activate venv-path)
               t)
           nil)))))
  :config
  (setq pyvenv-mode-line-indicator
        '(pyvenv-virtual-env-name ("[venv:" pyvenv-virtual-env-name "] ")))
  (pyvenv-mode +1))

(use-package js
  :straight (:type built-in)
  :mode ("\\.[c|m]?jsx?\\'" . js-mode)
  :init
  (setq js-indent-level 2)
  :config
  (add-to-list 'js-jsx-regexps "\\/.?>[^\"']"))

(use-package css-mode
  :straight (:type built-in)
  :init
  (setq css-indent-offset 2))

(use-package prisma-mode
  :mode "\\.prisma\\'"
  :straight (:host github :repo "pimeys/emacs-prisma-mode"))

(use-package lua-mode)

(use-package yaml-mode
  :mode ("\\.yml\\'"
         "\\.yaml\\'")
  :hook (yaml-mode . (lambda ()(setq evil-shift-width 2))))

(use-package ansible
  :commands ansible)

(use-package caddyfile-mode
  :mode ("Caddyfile\\'" "caddy\\.conf\\'")
  :init
  (add-hook
   'caddyfile-mode-hook
   (lambda ()(setq-local tab-width 4))))

(use-package csv-mode
  :mode ("\\.csv\\'")
  :config
  (setq csv-separators '("," ";" "|" "\t" " ")))

(use-package fish-mode
  :mode ("\\.fish\\'"))

(use-package mermaid-mode
  :mode ("\\.mmd\\'"))

(use-package dockerfile-mode
  :mode ("Dockerfile\\'"))
